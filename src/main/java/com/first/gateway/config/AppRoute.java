package com.first.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppRoute {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/prodotto/**")
                        .uri("http://localhost:8080"))

                .route(r -> r.path("/carrello/**")
                        .uri("http://localhost:8081"))
                .build();
    }
}
